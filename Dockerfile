FROM alpine:latest

RUN apk add --no-cache bash curl jq
COPY gandi-live-dns-updater.sh /

ENTRYPOINT [ "bash", "/gandi-live-dns-updater.sh" ]
