#!/bin/bash

INTERVAL=5
ENDPOINT="https://dns.api.gandi.net/api/v5"
TTL=300
URL="$ENDPOINT/domains/$DOMAIN/records/$RRNAME/A"

if [[ $DOMAIN == "" ]] || [[ $RRNAME == "" ]] || [[ $API_TOKEN == "" ]]; then
    echo "Not all required variables are defined. Aborting."
    exit 1
fi

# Fetch the IP address from our location
MY_IP=$(curl -s http://icanhazip.com | grep -E '([0-9]{1,3}\.?){4}')
if [[ "$MY_IP" == "" ]]; then
    echo "Failed to fetch the ip address. Aborting."
    exit 1
fi

trap "exit" INT
while true; do
    sleep $INTERVAL

    CURRENT_IP=$(curl -s "$URL" \
        -H "Content-Type: application/json" \
        -H "X-Api-Key: $API_TOKEN" | jq -r '.rrset_values[0]')

    if [[ "$MY_IP" == "$CURRENT_IP" ]]; then
        [ ${DEBUG:-"false"} != "false" ] && echo "Record up-to-date. Retrying in $INTERVAL seconds."
        continue
    fi

    curl -s -X PUT "$URL" \
        -H "X-Api-Key: $API_TOKEN" \
        -H "Content-Type: application/json" \
        -d "{\"rrset_ttl\": $TTL, \"rrset_values\": [\"$MY_IP\"]}"
done
